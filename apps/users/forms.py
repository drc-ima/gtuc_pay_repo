from django.contrib.auth.forms import UserCreationForm
from django.forms import *

from apps.users.models import GPUser


class LogoutForm(Form):
    pass


class CreateUserForm(UserCreationForm):
    date_of_birth = DateField(widget=TextInput(attrs={'type': 'date'}))

    class Meta(UserCreationForm.Meta):
        model = GPUser
        fields = UserCreationForm.Meta.fields + ('email', 'first_name', 'last_name', 'gender', 'date_of_birth')
