from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from apps.utils.random_stuff import *
from django.db import models
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from ..utils.random_stuff import invite_new_user_email


# Create your models here.
class GPUserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None, **extra_fields):
        if not email:
            raise ValueError('username is required')

        user = self.model(
            email=email,
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        user = self.create_user(email=email, first_name=first_name, last_name=last_name, password=password)
        user.is_superuser = True
        user.is_active = True
        user.is_staff = True
        user.username = user.email
        user.save(using=self._db)
        return user


USER_TYPES = {
    ('SAU', 'Student App User'),
    ('ASU', 'Accounts Office Super User'),
    ('AOU', 'Accounts Office User'),
}


SEX = {
    ('male', 'male'),
    ('female', 'female'),
    ('other', 'other')
}


class GPUser(AbstractBaseUser, PermissionsMixin):
    user_id = models.CharField(unique=True, default=generate_id, editable=False, max_length=5)
    username = models.CharField(
        _('Username'),
        max_length=255,
        blank=True,
        null=True,
    )
    email = models.EmailField(
        _('Email address'),
        unique=True,
    )
    first_name = models.CharField(_('First Name'), max_length=255, blank=True, null=True)
    last_name = models.CharField(_('Last Name'), max_length=255, blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255, blank=True, null=True)
    staff_id = models.CharField(_('Staff Id'), max_length=255, blank=True, null=True)
    user_profile = models.FileField(_('User Profile'), upload_to='media/', blank=True, null=True)
    gender = models.CharField(_('Gender'), choices=SEX, default='other', max_length=255, blank=True, null=True)
    student_id = models.CharField(_('Staff Id'), max_length=255, blank=True, null=True)
    is_active = models.BooleanField(_('Active Status'), default=True)
    is_staff = models.BooleanField(_('Staff Status'), default=False)
    is_superuser = models.BooleanField(_('Super User Status'), default=False)
    date_of_birth = models.DateField(_('Date of Birth'), default=timezone.now)
    login_number = models.IntegerField(_('Login Number'), default=0, blank=True, null=True)
    date_joined = models.DateTimeField(_('Date Joined'), default=timezone.now)
    user_type = models.CharField(_('User Type'), choices=USER_TYPES, max_length=255, default='SAU', blank=True, null=True)

    objects = GPUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def get_short_name(self):
        return self.first_name

    def get_dob(self):
        return self.date_of_birth

    class Meta:
        verbose_name_plural = 'GTUC Pay Users'
        verbose_name = 'user'
        ordering = ('id', 'user_id', 'date_joined')

    def __str__(self):
        return f'{self.email}'


# def invite_user(sender, instance, created, **kwargs):
#     if created and instance.user_type == 'ASU' or instance.user_type == 'AOU':
#         invite_new_user_email(sender, instance.user_id)
#
#
# post_save.connect(invite_user, sender=settings.AUTH_USER_MODEL)
