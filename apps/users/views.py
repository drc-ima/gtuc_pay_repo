from django.contrib.auth import logout, login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import *

from apps.pay.models import Payment
from apps.pay_admins.forms import ChangePasswordForm, UserChangeForm
from apps.pay_admins.models import Service
from apps.users.forms import LogoutForm, CreateUserForm
from apps.users.models import GPUser


class Dashboard(LoginRequiredMixin, ListView):
    paginate_by = 5

    def get_template_names(self):
        if self.request.user.user_type == 'ASU' or self.request.user.user_type == 'AOU':
            return 'admins/dashboard.html'
        else:
            return 'users/index.html'

    def get_queryset(self):
        if self.request.user.user_type == 'ASU' or self.request.user.user_type == 'AOU':
            return Payment.objects.all()
        else:
            return Service.objects.all()


class SignUpView(CreateView):
    form_class = CreateUserForm
    template_name = 'users/signup.html'
    success_url = reverse_lazy('users:login')

    def form_valid(self, form):
        valid = super().form_valid(form)
        password, email = form.cleaned_data.get('password1'), form.cleaned_data.get('email')
        form.instance.username = f'{email}'
        form.save()
        return valid


class LogoutView(LoginRequiredMixin, FormView):
    form_class = LogoutForm

    def get_template_names(self):
        if self.request.user.user_type == 'ASU' or self.request.user.user_type == 'AOU':
            return 'users/logout_admin.html'
        else:
            return 'users/logout.html'

    def form_valid(self, form):
        logout(self.request)
        return HttpResponseRedirect(reverse('home'))


class Profile(LoginRequiredMixin, UpdateView):
    model = GPUser
    form_class = UserChangeForm
    pk_url_kwarg = 'id'
    success_url = reverse_lazy('users:dashboard')

    def get_template_names(self):
        if self.request.user.user_type == 'ASU' or self.request.user.user_type == 'AOU':
            return 'users/profile1.html'

        else:
            return 'users/profile.html'


class ChangePassword(LoginRequiredMixin, FormView):
    form_class = ChangePasswordForm
    success_url = reverse_lazy('users:dashboard')

    def get_template_names(self):
        if self.request.user.user_type == 'ASU' or self.request.user.user_type == 'AOU':
            return 'registration/password_change_form1.html'
        else:
            return 'registration/password_change_form.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        return super().form_valid(form)