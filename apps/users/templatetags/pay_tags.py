from django import template

from apps.pay.forms import PayMomoForm
from apps.pay_admins.forms import ServiceForm, AdminCreationForm, ChangePasswordForm, FacultyForm

register = template.Library()


@register.inclusion_tag('admins/dashboard.html')
def admin_dashboard():
    pass


@register.inclusion_tag('admins/first_dash.html')
def first_dashboard():
    pass


@register.inclusion_tag('admins/first_dash.html')
def change_password():
    form = ChangePasswordForm
    return {'form': form}


@register.inclusion_tag('services/_forms.html')
def service_form():
    form = ServiceForm
    return {'form': form}


@register.inclusion_tag('payment/_momo.html')
def momo_form():
    form = PayMomoForm
    return {'form': form}


@register.inclusion_tag('users/_forms.html')
def user_form():
    form = AdminCreationForm
    return {'form': form}

@register.inclusion_tag('faculties/_forms.html')
def faculty_form():
    form = FacultyForm
    return {'form': form}