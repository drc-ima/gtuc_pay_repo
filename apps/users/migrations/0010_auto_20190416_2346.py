# Generated by Django 2.2 on 2019-04-16 23:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20190416_2304'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gpuser',
            name='user_type',
            field=models.CharField(blank=True, choices=[('ASU', 'Accounts Office Super User'), ('SAU', 'Student App User'), ('AOU', 'Accounts Office User')], default='SAU', max_length=255, null=True, verbose_name='User Type'),
        ),
    ]
