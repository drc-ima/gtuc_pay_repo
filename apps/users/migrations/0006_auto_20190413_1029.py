# Generated by Django 2.2 on 2019-04-13 10:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20190413_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gpuser',
            name='gender',
            field=models.CharField(blank=True, choices=[('female', 'female'), ('male', 'male'), ('other', 'other')], default='other', max_length=255, null=True, verbose_name='Gender'),
        ),
        migrations.AlterField(
            model_name='gpuser',
            name='user_type',
            field=models.CharField(blank=True, choices=[('ASU', 'Accounts Office Super User'), ('AOU', 'Accounts Office User'), ('SAU', 'Student App User')], default='SAU', max_length=255, null=True, verbose_name='User Type'),
        ),
    ]
