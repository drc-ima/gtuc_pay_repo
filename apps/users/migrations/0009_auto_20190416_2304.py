# Generated by Django 2.2 on 2019-04-16 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_auto_20190416_2254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gpuser',
            name='gender',
            field=models.CharField(blank=True, choices=[('female', 'female'), ('male', 'male'), ('other', 'other')], default='other', max_length=255, null=True, verbose_name='Gender'),
        ),
        migrations.AlterField(
            model_name='gpuser',
            name='user_type',
            field=models.CharField(blank=True, choices=[('SAU', 'Student App User'), ('AOU', 'Accounts Office User'), ('ASU', 'Accounts Office Super User')], default='SAU', max_length=255, null=True, verbose_name='User Type'),
        ),
    ]
