# Generated by Django 2.2 on 2019-04-08 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20190408_1020'),
    ]

    operations = [
        migrations.AddField(
            model_name='gpuser',
            name='login_number',
            field=models.IntegerField(blank=True, default=0, null=True, verbose_name='Login Number'),
        ),
        migrations.AlterField(
            model_name='gpuser',
            name='gender',
            field=models.CharField(blank=True, choices=[('female', 'female'), ('male', 'male'), ('other', 'other')], default='other', max_length=255, null=True, verbose_name='Gender'),
        ),
        migrations.AlterField(
            model_name='gpuser',
            name='user_type',
            field=models.CharField(blank=True, choices=[('AOU', 'Accounts Office User'), ('SAU', 'Student App User'), ('ASU', 'Accounts Office Super User')], default='SAU', max_length=255, null=True, verbose_name='User Type'),
        ),
    ]
