from django.urls import path, include
from django.views.generic import TemplateView

from apps.users.views import *

app_name = 'users'


urlpatterns = [
    path('dashboard/', Dashboard.as_view(), name='dashboard'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('signup/', SignUpView.as_view(), name='signup'),
    # path('layout/admin/', TemplateView.as_view(template_name='admin_layout.html')),
    path('password/', ChangePassword.as_view(), name='password'),
    path('profile/<id>', Profile.as_view(), name='profile'),
    path('', include('django.contrib.auth.urls')),



]
