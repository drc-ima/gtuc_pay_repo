from random import randrange

from django.core.mail import EmailMessage, send_mail
from django.http import HttpRequest
from django.shortcuts import render
from django.utils import timezone

from django.conf import settings


def generate_id():
    numbers = '0123456789'
    length = 5
    user_id = ""
    for i in range(length):
        user_id += numbers[randrange(0, len(numbers))]
    return f'{user_id}'


def generate_password():
    FROM = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    LENGTH = 10
    password = ""
    for i in range(LENGTH):
        password += FROM[randrange(0, len(FROM))]
    return f'{password}'


def unique_token():
    FROM = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    LENGTH = 15
    password = ""
    for i in range(LENGTH):
        password += FROM[randrange(0, len(FROM))]
    return f'{password}'


def send_email(email, content):
    email = EmailMessage('New User', content, from_email='rax.cubi@gmail.com', to=[email,], reply_to=['rax.cubi@gmail.com'])
    email.content_subtype = "html"
    email.send()


def invite_new_user_email(user_class, user_id):
    try:
        new_user = user_class.objects.get(user_id=user_id)
    except user_class.DoesNotExist:
        pass
    else:
        request = HttpRequest()
        html = render(request, 'emails/new_user.html', context={'new_user': new_user})
        text = render(request, 'emails/new_user.txt', context={'new_user': new_user})
        send_mail(
            subject='New User',
            message=text.content.decode('utf-8'),
            from_email='rax.cubi@gmail.com',
            recipient_list=[new_user.email, ],
            html_message=html.content.decode('utf-8')
        )

