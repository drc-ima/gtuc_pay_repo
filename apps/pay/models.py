from django.db import models

from apps.pay_admins.models import Service, Faculty
from apps.utils.random_stuff import *
from django.conf import settings
# # Create your models here.


DEBIT_TYPES = {
    ('MTN', 'MTN Mobile Money'),
    ('Voda', 'Vodafone Cash'),
    ('Tigo', 'Tigo Cash'),
    ('Air', 'Airtel Money'),
}

PROGRAMMES = {
    ('BIT', 'Bsc. Information Technology'),
    ('BCE', 'Bsc. Computer Engineering'),
    ('BTE', 'Bsc. Telecommunication Engineering'),
    ('BF', 'Bsc. Banking & Financial'),
    ('BM', 'Bsc. Management'),
    ('BA', 'Bsc. Accounting'),
    ('BMk', 'Bsc. Marketing'),
    ('BE', 'Bsc. Economics'),
    ('BHR', 'Bsc. Human Resource Management'),
    ('BAC', 'Bsc. Accounting with Computing'),
    ('BPL', 'Bsc. Procurement and Logistics'),

}

LEVEL = {
    ('100', '100'),
    ('200', '200'),
    ('300', '300'),
    ('400', '400'),
}


class Payment(models.Model):
    student_full_name = models.CharField(max_length=255, blank=True, null=True)
    # student_index_number = models.CharField(max_length=255, blank=True, null=True)
    student_faculty = models.ForeignKey(Faculty, related_name='payments', on_delete=models.SET_NULL, blank=True,
                                        null=True)
    student_programme = models.CharField(choices=PROGRAMMES, blank=True, null=True, max_length=255,)
    level = models.CharField(choices=LEVEL, blank=True, null=True, max_length=255,)
    service_to_pay = models.ForeignKey(Service, related_name='services', on_delete=models.SET_NULL, blank=True,
                                       null=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    balance = models.CharField(max_length=255, blank=True, null=True)
    student_id = models.CharField(
        max_length=255,
        help_text='Enter the student ID of the student you are paying fees for',
        blank=True,
        null=True
    )
    pay_with = models.CharField(choices=DEBIT_TYPES, max_length=255, blank=True, null=True)
    is_card = models.BooleanField(default=False)
    card_number = models.CharField(max_length=255, blank=True, null=True)
    cvv = models.IntegerField(blank=True, null=True)
    expiry_date = models.DateField(blank=True, null=True)
    mobile_number = models.CharField(max_length=255, blank=True, null=True)
    paid_at = models.DateTimeField(default=timezone.now)
    paid_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='dues_fees',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    token = models.CharField(default=unique_token, max_length=255, unique=True)
    is_verified = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.student_full_name}, {self.amount}'

    class Meta:
        ordering = ['-paid_at']


#
#
#
# class Accounts(models.Model):
#     account_name = models.CharField(max_length=255, blank=True, null=True)
#     account_number = models.CharField(max_length=255, blank=True, null=True)
#     bank = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         abstract = True
#
#
# class Association(models.Model):
#     name = models.CharField(max_length=255, blank=True, null=True)
#     faculty = models.ForeignKey(
#         Faculty,
#         related_name='association_faculty',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#     logo = models.FileField(upload_to='media/', blank=True, null=True)
#     created_at = models.DateTimeField(default=timezone.now)
#     created_by = models.ForeignKey(
#         settings.AUTH_USER_MODEL,
#         related_name='association',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#
#     def __str__(self):
#         return f'{self.name}'
#
#
# class SchoolAccounts(Accounts):
#     fees = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
#
#
# class AssociationAccounts(Accounts):
#     fees = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True)
#
#

#
#
# class PayFees(models.Model):
#     token = models.CharField(max_length=255, blank=True, null=True)
#     student_id = models.CharField(
#         max_length=255,
#         help_text='Enter the student ID of the student you are paying fees for',
#         blank=True,
#         null=True
#     )
#     fees_account = models.ForeignKey(
#         SchoolAccounts,
#         related_name='fees_accounts',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#     amount = models.DecimalField(max_digits=15, decimal_places=2)
#     debit_account_type = models.CharField(choices=DEBIT_TYPES, max_length=255, blank=True, null=True)
#     card_number = models.CharField(max_length=255, blank=True, null=True)
#     cvv = models.IntegerField(blank=True, null=True)
#     expiry_date = models.DateField()
#     mobile_number = models.CharField(max_length=255, blank=True, null=True)
#     paid_at = models.DateTimeField(default=timezone.now)
#     paid_by = models.ForeignKey(
#         settings.AUTH_USER_MODEL,
#         related_name='pay_fees',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#
#
# class DuesPayments(models.Model):
#     token = models.CharField(max_length=255, blank=True, null=True)
#     association = models.ForeignKey(
#         Association,
#         related_name='association',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#     student_id = models.CharField(
#         max_length=255,
#         help_text='Enter the student ID of the student you are paying fees for',
#         blank=True,
#         null=True
#     )
#     fees_account = models.ForeignKey(
#         SchoolAccounts,
#         related_name='dues_accounts',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#     amount = models.DecimalField(max_digits=15, decimal_places=2)
#     debit_account_type = models.CharField(choices=DEBIT_TYPES, max_length=255, blank=True, null=True)
#     card_number = models.CharField(max_length=255, blank=True, null=True)
#     cvv = models.IntegerField(blank=True, null=True)
#     expiry_date = models.DateField()
#     mobile_number = models.CharField(max_length=255, blank=True, null=True)
#     paid_at = models.DateTimeField(default=timezone.now)
#     paid_by = models.ForeignKey(
#         settings.AUTH_USER_MODEL,
#         related_name='dues_fees',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#
#
# class Complaint(models.Model):
#     from_user = models.ForeignKey(
#         settings.AUTH_USER_MODEL,
#         related_name='from_users',
#         on_delete=models.SET_NULL,
#         blank=True,
#         null=True
#     )
#     subject = models.CharField(max_length=255, blank=True, null=True)
#     message = models.TextField(max_length=400, null=True, blank=True)
#     shots = models.FileField(upload_to='media/', blank=True, null=True)
#     complaint_date = models.DateTimeField(default=timezone.now)
#
#     def __str__(self):
#         return f'{self.from_user}, {self.subject}'
#
#     class Meta:
#         verbose_name_plural = 'Complaints'
#
