from django.urls import *
from .views import *

app_name = 'pay'


urlpatterns = [
    path('service/detail/<id>/', ServiceDetail.as_view(), name='service-detail'),
    path('momo/<id>/', PayMomo.as_view(), name='pay-momo'),
    path('card/<id>/', PayCard.as_view(), name='pay-card'),
    path('momo/confirm/<id>/', ConfirmMomoPay.as_view(), name='confirm-momo'),
    path('card/confirm/<id>/', ConfirmCardPay.as_view(), name='confirm-card'),
    path('cancel/<id>/', CancelPayment.as_view(), name='cancel'),
    path('success/<id>/', SuccessPayment.as_view(), name='success'),
    path('transactions/', UserPayment.as_view(), name='transactions'),
    path('transaction/detail/<id>/', PaymentDetail.as_view(), name='transaction-detail'),
    path('support/', Support.as_view(), name='support'),
]
