from django.forms import *
from . models import *
from apps.pay_admins.models import *


class PaymentForm(ModelForm):

    class Meta:
        model = Payment
        fields = ('student_full_name', 'student_faculty', 'student_id', 'service_to_pay', 'amount', 'pay_with',
                  'mobile_number', 'card_number', 'cvv', 'expiry_date')


class PayMomoForm(ModelForm):

    class Meta:
        model = Payment
        fields = ('student_full_name', 'student_id', 'student_faculty', 'amount', 'pay_with', 'mobile_number', 'level',
                  'student_programme')


class PayCardForm(ModelForm):
    expiry_date = DateField(widget=TextInput(attrs={'type': 'date'}))

    class Meta:
        model = Payment
        fields = ('student_full_name', 'student_id', 'student_faculty', 'amount', 'card_number',
                  'cvv', 'expiry_date', 'level', 'student_programme')


class ComplaintForm(ModelForm):
    class Meta:
        model = Compliant
        fields = ('subject', 'message',)
