# Generated by Django 2.1.7 on 2019-04-04 04:14

import apps.utils.random_stuff
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Complaint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(blank=True, max_length=255, null=True)),
                ('message', models.TextField(max_length=400)),
                ('shots', models.FileField(upload_to='media/')),
                ('complaint_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_full_name', models.CharField(blank=True, max_length=255, null=True)),
                ('student_index_number', models.CharField(blank=True, max_length=255, null=True)),
                ('amount', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True)),
                ('student_id', models.CharField(blank=True, help_text='Enter the student ID of the student you are paying fees for', max_length=255, null=True)),
                ('pay_with', models.CharField(blank=True, choices=[('Air', 'Airtel Money'), ('Card', 'VISA/MasterCard'), ('Voda', 'Vodafone Cash'), ('MTN', 'MTN Mobile Money'), ('Tigo', 'Tigo Cash')], max_length=255, null=True)),
                ('card_number', models.CharField(blank=True, max_length=255, null=True)),
                ('cvv', models.IntegerField(blank=True, null=True)),
                ('expiry_date', models.DateField()),
                ('mobile_number', models.CharField(blank=True, max_length=255, null=True)),
                ('paid_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('token', models.CharField(default=apps.utils.random_stuff.unique_token, max_length=255, unique=True)),
            ],
        ),
    ]
