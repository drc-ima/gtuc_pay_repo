import num2words
from django.urls import reverse
from apps.pay_admins.views import *
from django.views.generic import *
from . forms import *
from . import models
# Create your views here.


class UserPayment(LoginRequiredMixin, ListView):
    model = models.Payment
    template_name = 'payment/transactions.html'
    paginate_by = 5

    def get_queryset(self):
        return models.Payment.objects.filter(paid_by=self.request.user).order_by('-paid_at')


class PaymentDetail(LoginRequiredMixin, DetailView):
    model = models.Payment
    queryset = models.Payment.objects.all()
    template_name = 'payment/transactions-detail.html'
    pk_url_kwarg = 'id'


class ServiceDetail(LoginRequiredMixin, DetailView):
    model = models.Payment
    template_name = 'payment/service_detail.html'
    pk_url_kwarg = 'id'
    queryset = Service.objects.all()


class PayMomo(LoginRequiredMixin, FormView):
    form_class = PayMomoForm
    template_name = 'payment/pay_option.html'

    def get_success_url(self):
        return reverse('pay:confirm-momo', kwargs={
            'id': self.token.id
        })
        # return reverse_lazy('pay:success')

    def get_token(self, id):
        self.token = models.Payment.objects.get(id=id)
        return self.token

    def get_object(self):
        self.object = Service.objects.get(id=self.kwargs.get('id'))
        return self.object

    def get_queryset(self):
        return Service.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.get_object()
        context['token'] = self.get_token
        return context

    def form_valid(self, form):
        pay = models.Payment.objects.create(
            student_id=form.instance.student_id,
            student_full_name=form.instance.student_full_name,
            student_faculty=form.instance.student_faculty,
            student_programme=form.instance.student_programme,
            level=form.instance.level,
            service_to_pay=self.get_object(),
            amount=form.instance.amount,
            pay_with=form.instance.pay_with,
            mobile_number=form.instance.mobile_number,
            # balance=int(form.instance.amount) - int(form.instance.service_to_pay.amount),
            paid_by=self.request.user,
        )
        pay.balance = f'{pay.amount - pay.service_to_pay.amount}'
        pay.save()
        self.get_token(pay.id)
        return super(PayMomo, self).form_valid(form)


class PayCard(LoginRequiredMixin, FormView):
    form_class = PayCardForm
    template_name = 'payment/pay_option_card.html'

    def get_success_url(self):

        return reverse('pay:confirm-card', kwargs={
            'id': self.token.id
        })
        # return reverse_lazy('pay:success')

    def get_token(self, id):
        self.token = models.Payment.objects.get(id=id)
        return self.token

    def get_object(self):
        self.object = Service.objects.get(id=self.kwargs.get('id'))
        return self.object

    def get_queryset(self):
        return Service.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.get_object()
        context['token'] = self.get_token
        return context

    def form_valid(self, form):
        pay = models.Payment.objects.create(
            student_id=form.instance.student_id,
            student_full_name=form.instance.student_full_name,
            student_faculty=form.instance.student_faculty,
            service_to_pay=self.get_object(),
            student_programme=form.instance.student_programme,
            level=form.instance.level,
            amount=form.instance.amount,
            card_number=form.instance.card_number,
            cvv=form.instance.cvv,
            expiry_date=form.instance.expiry_date,
            # balance=int(form.instance.amount) - int(form.instance.service_to_pay.amount),
            paid_by=self.request.user,
        )
        pay.balance = f'{pay.amount - pay.service_to_pay.amount}'
        pay.is_card = True
        pay.save()
        self.get_token(pay.id)
        return super(PayCard, self).form_valid(form)


class ConfirmMomoPay(LoginRequiredMixin, UpdateView):
    model = models.Payment
    form_class = PayMomoForm
    template_name = 'payment/edit_momo.html'
    pk_url_kwarg = 'id'
    success_url = reverse_lazy('users:dashboard')

    def form_valid(self, form):
        message = f'Hi {form.instance.paid_by.get_full_name()}, your payment for ' \
                  f'{form.instance.service_to_pay.service_name} at ' \
                  f'{form.instance.service_to_pay.currency} {form.instance.amount} ' \
                  f'was successful. The token ' \
                  f'of the payment is {form.instance.token}. Use this token to verify your payment'
        send_email(form.instance.paid_by.email, message)
        return super(ConfirmMomoPay, self).form_valid(form)


class ConfirmCardPay(LoginRequiredMixin, UpdateView):
    model = models.Payment
    form_class = PayCardForm
    template_name = 'payment/edit_card.html'
    pk_url_kwarg = 'id'
    success_url = reverse_lazy('users:dashboard')

    def form_valid(self, form):
        message = f'Hi {form.instance.paid_by.get_full_name()}, your payment for ' \
                  f'{form.instance.service_to_pay.service_name} at ' \
                  f'{form.instance.service_to_pay.currency} {form.instance.amount} ' \
                  f'was successful. The token ' \
                  f'of the payment is {form.instance.token}. Use this token to verify your payment'
        send_email(form.instance.paid_by.email, message)
        return super(ConfirmCardPay, self).form_valid(form)


class CancelPayment(LoginRequiredMixin, DeleteView):
    model = models.Payment
    queryset = Payment.objects.all()
    pk_url_kwarg = 'id'
    success_url = reverse_lazy('users:dashboard')


class SuccessPayment(LoginRequiredMixin, DetailView):
    model = models.Payment
    template_name = 'payment/success.html'
    queryset = models.Payment.objects.all()
    pk_url_kwarg = 'id'


class Support(LoginRequiredMixin, CreateView):
    model = Compliant
    template_name = 'faculties/support.html'
    queryset = Compliant.objects.all()
    success_url = reverse_lazy('users:dashboard')
    form_class = ComplaintForm

    def form_valid(self, form):
        form.instance.report_by = self.request.user
        return super(Support, self).form_valid(form)
