from django.forms import forms

from apps.users.models import GPUser
from . models import *
from django.contrib.auth.forms import *
from django.conf import settings


class AdminCreationForm(forms.ModelForm):

    class Meta:
        model = GPUser
        fields = ('email', 'first_name', 'last_name', 'staff_id')


class UserChangeForm(forms.ModelForm):

    class Meta:
        model = GPUser
        fields = ('email', 'first_name', 'last_name', 'user_profile', 'date_of_birth', 'gender')


class ChangePasswordForm(PasswordChangeForm):
    pass


class ServiceForm(forms.ModelForm):

    class Meta:
        model = Service
        fields = ('service_name', 'account_number', 'bank_name', 'currency', 'amount', 'logo')


class FacultyForm(forms.ModelForm):
    class Meta:
        model = Faculty
        fields = ('name',)
