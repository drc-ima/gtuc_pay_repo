from django.apps import AppConfig


class PayAdminsConfig(AppConfig):
    name = 'apps.pay_admins'
