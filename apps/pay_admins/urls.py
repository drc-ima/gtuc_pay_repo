from django.urls import path, include
from .views import *


app_name = 'pay_admins'


urlpatterns = [
    path('services/create/', CreateService.as_view(), name='service-create'),
    path('faculties/create/', CreateFaculty.as_view(), name='faculty-create'),
    path('services/list/', ListServices.as_view(), name='service-list'),
    path('faculties/create/', CreateFaculty.as_view(), name='faculty-create'),
    # path('faculties/list/', ListFaculty.as_view(), name='faculty-list'),
    path('sub/create/', CreateSubAdmin.as_view(), name='create'),
    # path('payment/list/', ListPayments.as_view(), name='payments-list'),
    path('payment/verify/<token>/<status>/', Verify.as_view(), name='payment-verify'),
    path('payment/cancel/<token>/<status>/', Cancel.as_view(), name='payment-cancel'),
    path('payment/search/', search, name='payment-search'),
    path('settings/', UserList.as_view(), name='settings'),
    path('complaint/resolve/<id>/<status>/', Resolve.as_view(), name='resolve'),
    path('profile/<id>/', Profile.as_view(), name='profile'),
    path('sub/active/<code>/<status>/', ActiveAccount.as_view(), name='active'),
    path('sub/deactive/<code>/<status>/', DeActiveAccount.as_view(), name='deactive'),
    path('service/update/<id>/', EditService.as_view(), name='service-update'),
    path('', include('django.contrib.auth.urls')),
    # path('users/detail/<user_id>/', UserDetail.as_view(), name='users-detail'),
    path('users/<user_id>/', UserDetails.as_view(), name='user-detail'),
]