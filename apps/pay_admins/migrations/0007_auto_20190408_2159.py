# Generated by Django 2.2 on 2019-04-08 21:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pay_admins', '0006_service_currency'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='currency',
            field=models.CharField(blank=True, choices=[('USD', 'US Dollars'), ('GHS', 'Ghana Cedis'), ('GBP', 'Pound Steling')], max_length=255, null=True),
        ),
    ]
