from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import Q
from django.shortcuts import get_object_or_404, render_to_response, render, redirect
from django.urls import reverse_lazy
from apps.pay.models import Payment
from apps.utils.random_stuff import *
from .forms import *
from django.views.generic import *


class CreateService(LoginRequiredMixin, CreateView):
    form_class = ServiceForm
    # template_name = 'services/_forms.html'
    success_url = reverse_lazy('admins:service-list')
    # queryset = Service.objects.all()

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.is_active = True
        return super(CreateService, self).form_valid(form)


class ListServices(LoginRequiredMixin, ListView):
    queryset = Service.objects.all()
    template_name = 'admins/services_list.html'
    model = Service


class EditService(LoginRequiredMixin, UpdateView):
    model = Service
    form_class = ServiceForm
    template_name = 'admins/service_update.html'
    success_url = reverse_lazy('admins:service-list')
    pk_url_kwarg = 'id'


class CreateFaculty(LoginRequiredMixin, CreateView):
    form_class = FacultyForm
    template_name = 'faculties/_forms.html'
    success_url = reverse_lazy('admins:settings')
    queryset = Faculty.objects.all()

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CreateFaculty, self).form_valid(form)


# class SearchPayment(LoginRequiredMixin, CreateView):
#     template_name = 'admins/dashboard.html'
#     queryset = Payment.objects.all()
#     model = Payment
#     fields = ['token',]
#     success_url = reverse_lazy('users:dashboard')
#
#     def get(self, request, *args, **kwargs):
#         search = request.GET.get('search', None)
#
#         if search is not None:
#             results = self.get_queryset()
#             for term in search.split():
#                 results = results.filter(Q(token__contains=term))

@login_required()
def search(request):
    if request.method == 'GET':
        query = request.GET.get('search', None)
        button = request.GET.get('submit')
        if query is not None:
            lookups = Q(token__icontains=query)

            results = Payment.objects.filter(lookups).distinct()
            context = {
                'results': results,
                'button': button
            }
            return render(request, 'users/index.html', context)
    else:
        return render('users/index.html')


class CreateSubAdmin(LoginRequiredMixin, CreateView):
    form_class = AdminCreationForm
    template_name = 'users/_forms.html'
    model = GPUser
    queryset = GPUser.objects.all()
    success_url = reverse_lazy('admins:settings')

    def form_valid(self, form):
        password = generate_password()
        form.instance.set_password(password)
        form.instance.username = form.instance.email
        message = f'<img src="https://images.unsplash.com/photo-1550867458-8181a6336615?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQ' \
                  f'iOjEyMDd9&auto=format&fit=crop&w=967&q=80" height="300" width="600" /> <p>Hi ' \
                  f'{form.instance.get_full_name()}</p> ' \
                  f'<p>You have been invited to be an admin on GTUC Pay Admin Portal. Please use the ' \
                  f'following credentials to login at <a href="http://localhost:8000/users/login/"> GTUC Pay Admin</a>' \
                  f'</p><p>Email : {form.instance.email} <br>Password : {password}</p><p>You are welcome,' \
                  f'<br>Moon Knights!</p>'
        send_email(form.instance.email, message)
        form.instance.user_type = 'AOU'
        return super(CreateSubAdmin, self).form_valid(form)


class ChangePassword(FormView):
    form_class = ChangePasswordForm
    template_name = 'admins/_password.html'
    success_url = reverse_lazy('users:dashboard')
    model = GPUser

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)

        return super().form_valid(form)


class UserList(LoginRequiredMixin, ListView):
    template_name = 'admins/preference.html'
    queryset = GPUser.objects.all()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['app_users'] = GPUser.objects.filter(user_type='SAU')
        context['admins'] = GPUser.objects.exclude(user_type='SAU')
        context['complaints'] = Compliant.objects.filter(is_resolve=False)
        return context


class UserDetails(LoginRequiredMixin, DetailView):
    model = GPUser
    queryset = GPUser.objects.all()
    context_object_name = 'user_object'
    template_name = 'admins/users-details.html'
    pk_url_kwarg = 'user_id'
    
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['payments'] = Payment.objects.filter()
    #     return context


class Resolve(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('users:dashboard')

    def get(self, request, *args, **kwargs):
        resolve = get_object_or_404(
            Compliant,
            id=kwargs.get('id'),
            is_resolve=False
        )
        if kwargs.get('status') == 'resolved':
            resolve.is_resolve = True
        else:
            resolve.is_resolve = False
        resolve.save()
        return super().get(request, *args, **kwargs)


class Verify(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('users:dashboard')

    def get(self, request, *args, **kwargs):
        verify = get_object_or_404(
            Payment,
            token=kwargs.get('token'),
            is_verified=False
        )
        if kwargs.get('status') == 'verified':
            verify.is_verified = True
            message = f'Dear {verify.paid_by.get_full_name()}, your fees payment for ' \
                      f'{verify.service_to_pay.service_name} has been verified. Amount is {verify.amount}'
            send_email(verify.paid_by.email, message)
        else:
            verify.is_verified = False
        verify.save()
        return super().get(request, *args, **kwargs)


class ActiveAccount(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('admins:settings')

    def get(self, request, *args, **kwargs):
        active = get_object_or_404(
            GPUser,
            user_id=kwargs.get('code'),
            is_active=False
        )
        if kwargs.get('status') == 'active':
            active.is_active = True
        else:
            active.is_active = False
        active.save()
        return super().get(request, *args, **kwargs)


class DeActiveAccount(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('admins:settings')

    def get(self, request, *args, **kwargs):
        deactive = get_object_or_404(
            GPUser,
            user_id=kwargs.get('code'),
            is_active=True
        )
        if kwargs.get('status') == 'deactive':
            deactive.is_active = False
        else:
            deactive.is_active = True
        deactive.save()
        return super().get(request, *args, **kwargs)


class Cancel(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('users:dashboard')

    def get(self, request, *args, **kwargs):
        verify = get_object_or_404(
            Payment,
            token=kwargs.get('token'),
            is_verified=True
        )
        if kwargs.get('status') == 'cancel':
            verify.is_verified = False
            message = f'Dear {verify.paid_by.get_full_name()}, your fees payment verification for ' \
                      f'{verify.service_to_pay.service_name} has been canceled. Amount is {verify.amount}'
            send_email(verify.paid_by, message)
        else:
            verify.is_verified = True
        verify.save()
        return super().get(request, *args, **kwargs)


class Profile(LoginRequiredMixin, UpdateView):
    model = GPUser
    form_class = UserChangeForm
    pk_url_kwarg = 'id'
    template_name = 'users/profile.html'
    success_url = reverse_lazy('users:dashboard')

    # def form_valid(self, form):
    #     post = form.save(commit=False)
    #     post.updated_by = self.request.user
    #     post.updated_at = timezone.now()
    #     post.save()
    #     return post

