from django.conf import settings
from django.db import models

# Create your models here.
from django.utils import timezone


CURRENCY = {
    ('GHS', 'Ghana Cedis'),
    ('USD', 'US Dollars'),
    ('GBP', 'Pound Steling')

}


class Service(models.Model):
    service_name = models.CharField(max_length=255, blank=True, null=True)
    account_number = models.CharField(max_length=255, blank=True, null=True)
    bank_name = models.CharField(max_length=255, blank=True, null=True)
    currency = models.CharField(choices=CURRENCY, max_length=255, blank=True, null=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    logo = models.FileField(upload_to='logo/', blank=True, null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=False)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return f'{self.service_name} - {self.amount}'


class Faculty(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='faculties',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['created_at']


class Compliant(models.Model):
    report_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='complaints', on_delete=models.SET_NULL,
                                  blank=True, null=True)
    subject = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField(max_length=255, blank=True, null=True)
    is_resolve = models.BooleanField(default=False)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.report_by}'

    class Meta:
        verbose_name_plural = 'Complaints'
