"""gtuc_pay_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from apps.pay_admins import urls as admin_urls
from apps.users import urls as user_urls
from gtuc_pay_api import settings
from apps.pay import urls as pay_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admins/', include(admin_urls, namespace='admins')),
    path('users/', include(user_urls, namespace='users')),
    path('pay/', include(pay_urls, namespace='pay')),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
